///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 05a - Animal Farm 2
/////
///// @file test.cpp
///// @version 1.0
/////
///// Test harness 
/////
///// @author Alexander Sidelev <asidelev@hawaii.edu>
///// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
///// @date   03_23_2021
/////////////////////////////////////////////////////////////////////////////////

#include <iostream>
//#include <list>
//#include <array>
#include <time.h>
//#include <bits/stdc++.h>
#include "node.hpp"
#include "list.hpp"
#include "animal.hpp" 
#include "animal_factory.hpp"
#include <cassert>

using namespace animalfarm;
//using namespace std;

int main() {
//   Node node; //instatntiate a node
   SingleLinkedList list; // instantinate a list
   assert(list.empty() == true);  
   assert(list.pop_front() == nullptr);
   //create a node ptr to a random animal
   Node* node = (Node*)AnimalFactory::randomAnimal();
   //push a node to the front of the list
   list.push_front(node);
   assert(list.empty() == false);

   //take out a node from the front of the list
   Node* newNode = list.pop_front();
   assert(list.empty() == true);
   assert(newNode != nullptr);
   assert(newNode->next == nullptr);
   
   //check push_front 

   
   cout << "   Number of Elements:  " << list.size() << endl;

   list.push_front(node);
   cout << "   Number of Elements round 2:  " << list.size() << endl;

   Node* node2 = (Node*)AnimalFactory::randomAnimal();
   list.push_front(node2);
   cout << "   Number of Elements round 3:  " << list.size() << endl;

//   cout << "Random Animal = " << list.push_front(AniamlFactory::randomAnimal()) << endl;
   return 0;
} //main fxn

