///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// EE 205  - Object Oriented Programming
///// Lab 05a - Animal Farm 2
/////
///// @file randoName.cpp
///// @version 1.0
/////
///// Test harness 
/////
///// @author Alexander Sidelev <asidelev@hawaii.edu>
///// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
///// @date   03_02_2021
/////////////////////////////////////////////////////////////////////////////////
//
#include <iostream>
#include <cstdio>
#include <cstdlib> 
#include <string>
#include <time.h>
#include "animal.hpp"
using namespace animalfarm;
using namespace std;
// needs to be part of animal class
char rando_atoz();
char randoAtoZ();

//values from cpprefrence ASCII table
int asciiAtoZ_min = 65;
int asciiAtoZ_max = 90;

// random integer [65,90] that is then converted to capital letter A-Z 
char randoAtoZ() {
   //initiate random number in [65,90]
   srand(time(NULL));
   //assign random number from [65,90] to n
   int n1;
   n1 = (rand()%(asciiAtoZ_max + 1 - asciiAtoZ_min)) + asciiAtoZ_min; 
   //convert and return ASCII decimal to char
   return char(n1);
}

int ascii_a_to_z_min = 97;
int ascii_a_to_z_max = 122;

char rando_atoz(){
   srand(time(NULL));
   int n2 = (rand()%(ascii_a_to_z_max + 1 - ascii_a_to_z_min)) + ascii_a_to_z_min;
   return char(n2);
}


int main() {
   const int length  =  7;
   char randoName[length];
   randoName[0]   =  randoAtoZ();
   int i = 0;
   for( i = 1; i < length; i++) {
      randoName[i] = rando_atoz();
   }
   cout << randoName << endl;
   return 0;
}

