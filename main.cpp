///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals among other things
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03_23_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <list>
#include <array>
#include <time.h> 
#include <bits/stdc++.h>
#include "animal_factory.hpp"
#include "animal.hpp"
#include "fish.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp" 
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"
//#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;


int main() {
   cout << "Welcome to Animal Farm 4" << endl;
   SingleLinkedList animalList; //instantiate a SLL
 

   for( auto i = 0; i < 25; i++) {
      animalList.push_front(AnimalFactory::randomAnimal() );
   }//for-loop



   cout << endl;
   cout << " List of Animals" << endl;
   cout << "   is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of Elements: " << animalList.size() << endl;
   
   for( auto animal = animalList.get_first()
         ; animal != nullptr
         ; animal = animalList.get_next(animal )) {
      cout << ((Animal*)animal)->speak() << endl;
   }//for-loop2
   while (!animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();

      delete animal;
   }//while-loop
   

   }//main fxn


