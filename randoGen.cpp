///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// EE 205  - Object Oriented Programming
/////// Lab 05a - Animal Farm 2
///////
/////// @file randoGender.cpp
/////// @version 1.0
///////
/////// Test harness random gender
///////
/////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/////// @date   03_022021
///////////////////////////////////////////////////////////////////////////////////
////
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <time.h>
#include "animal.hpp"

using namespace std;
using namespace animalfarm;

enum Gender gender;

char randoGen(enum Gender gender) {
   srand(time(NULL));
   int n;
   n = (rand()%3);
   switch(gender){
      case 0: return "Male";
      case 1: return "Female";
      case 2: return "Unknown";
      default: return NULL;       
   }      
//   cout << n << endl; 
   return NULL;
}


