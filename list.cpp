///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// EE 205  - Object Oriented Programming
/////////// Lab 07a - Animal Farm 2
///////////
/////////// @file list.cpp
/////////// @version 1.0
///////////
/////////// Body file for the List class
///////////
/////////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////////// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/////////// @date   03_28_2021
///////////////////////////////////////////////////////////////////////////////////////

//#include <cstddef>
#include "list.hpp"
namespace animalfarm {

   const bool SingleLinkedList::empty() const {
  
      return head == nullptr;
   } //empty() fxn
  
 //  Node* head = new Node();   
   void SingleLinkedList::push_front(Node* newNode) {
      newNode->next = head;
      head = newNode;
   } // push_front()
   
   Node* SingleLinkedList::pop_front() {
      if ( head == nullptr){
         return nullptr;
      }
      Node* meow = head;
      head = head->next;
      meow->next = nullptr; 
      return meow;
   
   }//pop_front()

   Node* SingleLinkedList::get_first() const { 
      return head;
   }//get_first()
 
   Node* currentNode = new Node();

   Node* SingleLinkedList::get_next(const Node* currentNode ) const {
      
      return currentNode->next;
   }//get_next() fxn



   unsigned int SingleLinkedList::size() const {
      int count = 0;
      Node* current = head;
      while(current != nullptr) {
         count++;
         current = current->next;
      }//while loop
      return count;
   }
//   unsigned int size() {
//      unsigned int size = 0;
//      Node* node = head;
//      while(node != NULL) {
//         size++;
//         node = node->next;
//      }//while loop
//      return size;
//   }//size fxn

} // animalfarm

