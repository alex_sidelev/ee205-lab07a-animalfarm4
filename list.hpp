///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// EE 205  - Object Oriented Programming
/////////// Lab 07a - Animal Farm 2
///////////
/////////// @file list.hpp
/////////// @version 1.0
///////////
/////////// Header file for the list class
///////////
/////////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////////// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/////////// @date   03_28_2021
///////////////////////////////////////////////////////////////////////////////////////
//////
////
#pragma once
#include "node.hpp"
#include <cstdlib> 

namespace animalfarm {



class SingleLinkedList {
   public:
//   SingleLinkedList();
//   ~SingleLinkedList();
      const bool empty() const;
      void push_front(Node* newNode);
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      unsigned int size() const;
      
      //   Node* pop_front();

   protected:
      Node* head = nullptr;
}; // SingleLinkedList
}//animalfarm namespace
 
