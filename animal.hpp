///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   03_22_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp" 
using namespace std;

#include <string>

namespace animalfarm {

enum Gender { MALE, FEMALE, UNKNOWN };

enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN };  

class Animal: public Node   {
public:
   //constructor and destructor declarations
   Animal();
   ~Animal();
	enum Gender gender;
	string      species;

	virtual const string speak() = 0;

	void printInfo();	

	string colorName  (enum Color color);

	string genderName (enum Gender gender);

   static char rando_atoz();

   static char randoAtoZ();

   static enum Gender randomGen();

   static string randomName();

   static enum Gender getNewGender();

   static enum Color randomColor();

   static bool randomBool();

   static float randomWeight( const float from, const float to);



}; //class Animal

} // namespace animalfarm
