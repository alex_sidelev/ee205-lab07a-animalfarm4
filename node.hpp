///////////////////////////////////////////////////////////////////////////////
///////// University of Hawaii, College of Engineering
///////// EE 205  - Object Oriented Programming
///////// Lab 05a - Animal Farm 2
/////////
///////// @file node.hpp
///////// @version 1.0
/////////
///////// Header file for the Node class
/////////
///////// @author Alexander Sidelev <asidelev@hawaii.edu>
///////// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
///////// @date   03_23_2021
/////////////////////////////////////////////////////////////////////////////////////
////
//
#pragma once
namespace animalfarm {

class Node {
   public:
//      Node();
//      ~Node();
   public:
         Node* next = nullptr; 
         friend class SingleLinkedList;
}; //Node class
} //namespace animalfarm
//
//
