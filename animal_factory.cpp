///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// EE 205  - Object Oriented Programming
/////// Lab 05a - Animal Farm 2
///////
/////// @file animal_factory.cpp
/////// @version 1.0
///////
/////// creates new animal classes
///////
/////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/////// @date   03_08_2021
///////////////////////////////////////////////////////////////////////////////////
////
#include <time.h>
#include <string>
#include <iostream>
#include "animal_factory.hpp"
namespace animalfarm {
   

// definintion of the fxn 
Animal* AnimalFactory::randomAnimal() {
   Animal* newAnimal = NULL;
//   srand(time(NULL));
   int i = (rand()%6);
   switch (i) {
      
      case 0: newAnimal = new Cat    (Animal::randomName(), Animal::randomColor(), Animal::randomGen()); break;
      
      case 1: newAnimal = new Dog    (Animal::randomName(), Animal::randomColor(), Animal::randomGen()); break;
     
      case 2: newAnimal = new Palila (Animal::randomName(), YELLOW, Animal::randomGen() ); break;

      case 3: newAnimal = new Nunu   (Animal::randomBool(), RED, Animal::randomGen()); break;
      
      case 4: newAnimal = new Aku    (Animal::randomWeight(1.0, 16.0), SILVER, Animal::randomGen()); break;
      
      case 5: newAnimal = new Nene   (Animal::randomName(), BROWN, Animal::randomGen()); break;
   
   } // switch statement
   return newAnimal;
} // randomAnimal
}//namespcae animalfarm

