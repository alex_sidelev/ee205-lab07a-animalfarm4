///////////////////////////////////////////////////////////////////////////////
/////// University of Hawaii, College of Engineering
/////// EE 205  - Object Oriented Programming
/////// Lab 05a - Animal Farm 2
///////
/////// @file animal_factory.hpp
/////// @version 1.0
///////
/////// header for creating new animal classes
///////
/////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/////// @date   03_08_2021
///////////////////////////////////////////////////////////////////////////////////
////
#include "cat.hpp"
#include "dog.hpp"
#include "palila.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "nene.hpp"
#include "animal.hpp"
//include the entire list of cpp animals
namespace animalfarm{

class AnimalFactory {
      public:
         static Animal* randomAnimal(); //needs to defined and declared
}; //class Animal
} // namespace animlfarm
