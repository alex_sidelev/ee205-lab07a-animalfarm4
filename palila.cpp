///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   02_13_2021
///////////////////////////////////////////////////////////////////////////////
#include <string> 

#include <iostream>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila(string newFound, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Loxioides bailleui";
   featherColor = newColor;  
   whereFound = newFound; 
   isMigratory = false;                                              
}


   void Palila::printInfo() {
      cout << "Palila" << endl;
      cout << "   Where Found = [" << whereFound << "]" << endl;
      Bird::printInfo();
}

} // namespace animalfarm






